var accountSID = process.env.TWILIO_ACCOUNT_SID; // Your Accoutn SID from twilio.com/console
var authToken = process.env.TWILIO_AUTH_TOKEN; // Your Auth Token from twilio.com/console

const client = require('twilio')(accountSID, authToken, { lazyLoading: true });

const sendMessage = async (message, senderID) => {
    try {
        await client.messages.create({
            to: senderID,
            body: message,
            from: 'whatsapp:+0000000000'
        });

        console.log('Send to ' + senderID + ' correct');
    }
    catch(err) {
        console.log('Error: ' + err);
    }
};

module.exports = { sendMessage };