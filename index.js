require('dotenv').config();

const express = require('express');
const WA = require('./src/helper/message-sender');

const app = express();
const PORT = process.env.PORT || 3600;
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send('Server ON');
});

app.post('/whatsapp', async (req, res) => {
    console.log(req.body);
    WA.sendMessage(req.body.Body,'whatsapp:+0000000000');
});

app.listen(PORT, () => {
    console.log('Server On port: ' + PORT);
});