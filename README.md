# Twilio WhatsApp

>This is a start point to integrate twilio library to create a webhook receiver of messages and also send.

## How to start

> When you clone this repository, first install ***ngrok*** global to you npm.

`
npm install -g ngrok
`

>And then to install all dependencies, execute

`
npm install
`

> Then to set server online, execute

`
npm test
`

> Finally to set online the webhook, execute
`
ngrok http [PORT]
`